using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraRotation : MonoBehaviour
{
    //private float xRotation, yRotation;
    private float yRotation;
    [SerializeField]
    private Camera camera;
    [SerializeField]
    private float sensitivity = 1.0f;

    private PlayerControls controls;
    private Vector2 mouseLook;
    private float xRotation = 0f;

    private int malletID;
    
    private void Awake()
    {
        controls = new PlayerControls();
    }

    private void Look()
    {
        mouseLook = controls.MouseMovement.Look.ReadValue<Vector2>();

        float mouseX = mouseLook.x * sensitivity * Time.deltaTime;
        float mouseY = mouseLook.y * sensitivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90);

        yRotation -= mouseX;
        yRotation = Mathf.Clamp(yRotation, -90f, 90);
        
        transform.localRotation = Quaternion.Euler(xRotation, -yRotation, 0);
    }

    private void mouseClicked()
    {
        if (Mouse.current.leftButton.wasPressedThisFrame)
        {
            //Checks what the cursor is pointing at

            
        }
    }

    private void moveMallet()
    {


    
    }

    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }
    
    void Start()
    {
        //Make sure that the cursor does not go off screen when moving
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        Look();
    }
}
